package demo.example;
import java.util.Scanner;
import java.io.*;

public class JavaBasics {
    static Scanner sc; // Class scope variable
    public static void main(String[] args) {
        sc = new Scanner(System.in);
        // demoVariables();
        // demoUsingClasses();
        // demoSwitch();
        // firstFiveOdd();
        // firstFiveOddFor();
        // doWhileExample();
        demoArrays();
        sc.close();
    }

    public static void demoVariables(){
        System.out.println("Enter your name: ");
        String yourName = sc.nextLine(); // Method scope variable
        System.out.println("Hello\n" + yourName);
        System.out.println("Enter a number: ");
        int num = Integer.parseInt(sc.nextLine());
        System.out.println(num + " squared is " + getSquare(num));
        System.out.println(num + " square root is  " + Math.sqrt(num));
        if(num >= 70){
            System.out.println("Grade A");
        }
        else if(num >= 45){
            System.out.println("Grade B");
        }
        else{
            System.out.println("Resit");
        }
    }

    public static long getSquare(int n){
        return n * n;
    }

    public static void demoUsingClasses() {
        File f = new File("C:\\Windows\\bootstat.dat");
        long len = f.length();
        System.out.println("File is " + len + " bytes long.");
    }

    public static void demoSwitch() {
        System.out.print("Enter your month of birth (as a string): ");
        String month = sc.nextLine();

        switch (month.toLowerCase()) {

            case "december":
            case "january":
            case "february":
                System.out.println("You were born in winter.");
                break;

            case "march":
            case "april":
            case "may":
                System.out.println("You were born in spring.");
                break;

            case "june":
            case "july":
            case "august":
                System.out.println("You were born in summer.");
                break;

            case "september":
            case "october":
            case "november":
                System.out.println("You were born in autumn.");
                break;

            default:
                System.out.println("I have no idea when you were born!");
                break;
        }
    }

    public static void firstFiveOdd(){
        int num = 1;
        while(num <= 9){
            System.out.println("Num is " + num);
            num += 2;
        }
    }

    public static void firstFiveOddFor(){
        for(int i = 1; i <= 9; i += 2){
            System.out.println("i is " + i);
        }
    }

    public static void doWhileExample(){
        String city;
        do{
            System.out.print("Enter a city: ");
            city = sc.nextLine().toUpperCase();
        } while(!(city.equals("OSLO") || city.equals("BERGEN") || city.equals("TRONDHEIM")));
    }

    public static void demoArrays(){
        String[] names = {"Fred", "Sue", "Ann", "Tom"};
        for(int i = 0; i < names.length; i++){
            System.out.println(names[i]);
        }
        System.out.println("Using For Each");
        for(String name: names){
            System.out.println(name);
        }
    }
}
